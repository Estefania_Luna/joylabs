﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using static System.Net.Mime.MediaTypeNames;

namespace joyLabs
{
    class files
    {
        // Atributos
        public string path;


        //Constructor
        public files(string path)
        {
            this.path = path;
           
        }
        string source;
        string finalPath;
        public string[] functionFiles(string path)
        {
            string mypath;            
            string[] data;
            mypath = Directory.GetCurrentDirectory();
            Regex exp = new Regex(@"(.*)(joyLabs)");
            Match myMatch = exp.Match(mypath);

            if (myMatch.Success == true)
            {
                string source = myMatch.Groups[1].Value;
                string joyLabs = myMatch.Groups[2].Value;
                
                finalPath = source + joyLabs +"\\" + path;                
            }
            string text = System.IO.File.ReadAllText(finalPath);
            data = text.Split("\n");
            return data;

        }
       
    }
}
