﻿using System;
using System.Collections.Generic;

namespace joyLabs
{
    class Program
    {
        static void Main(string[] args)
        {
            //First Challenge  ------- Day 1: Chronal Calibration ---
            string pathFirst = @"txtfiles\numbers.txt";
            files fileFirst = new files(pathFirst);
            string[] numbers = fileFirst.functionFiles(pathFirst);
            chronalCalibration chronal_calibration = new chronalCalibration();
            int total = chronal_calibration.Operation(numbers);
            Console.WriteLine("\r\nFirst Challenge ----Day 1: Chronal Calibration ----\r\n");
            Console.WriteLine("The resulting frequency is:" + total+ "");


            //Second Challenge --------- Day 2: Inventory Management System -----
            string pathSecond = @"txtfiles\letters.txt";
            files fileSecond = new files(pathSecond);
            string[] letters = fileFirst.functionFiles(pathSecond);
            inventoryManagementSystem inventory = new inventoryManagementSystem();
            int checksum = inventory.letters(letters);
            Console.WriteLine("\r\n\r\nSecond Challenge ----Day 2: Inventory Management System ----\r\n");
            Console.WriteLine("The checksum is: " + checksum);


            //Third Challenge  ------- Day 4: Repose Record ---
            string pathThird = @"txtfiles\dates.txt";
            files fileThird = new files(pathThird);
            string[] dates = fileThird.functionFiles(pathThird);
            reposeRecord repose = new reposeRecord();
            Console.WriteLine("\r\n\r\nThird Challenge  ----Day 4: Repose Record ----\r\n");
            repose.driver(dates);

         




        }

    }
}
